<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Authenticator\Controller;

use Fittinq\Symfony\Authenticator\Controller\CallbackController;

class Configuration
{
    private ?CallbackController $controller = null;

    public function configure(): CallbackController
    {
        if (!$this->controller) {
            $this->controller = new CallbackController();
        }

        return $this->controller;
    }
}