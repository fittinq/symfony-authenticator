<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Authenticator\Authenticator\Header;

use Firebase\JWT\JWT;
use Fittinq\Symfony\Authenticator\Authenticator\HeaderAuthenticator;
use Fittinq\Symfony\Authenticator\Authenticator\JWTUserProvider;
use stdClass;
use Symfony\Component\HttpFoundation\Request;

class Configuration
{
    private ?HeaderAuthenticator $authenticator = null;

    public function configure(): HeaderAuthenticator
    {
        if (!$this->authenticator) {
            $this->authenticator = new HeaderAuthenticator(
                new JWTUserProvider("tests/jwt.key.pub")
            );
        }

        return $this->authenticator;
    }

    public function createRequestWithJwt(string $jwt): Request
    {
        $request = new Request();
        $request->headers->set("Authorization", $jwt);

        return $request;
    }

    public function createRequest(?string $username = null, ?array $roles = null, ?int $expiresAt = null): Request
    {
        $jwt = JWT::encode(
            $this->getPayload($username, $roles, $expiresAt),
            file_get_contents("tests/jwt.key"),
            'RS256'
        );

        $request = new Request();
        $request->headers->set("Authorization", "Bearer {$jwt}");

        return $request;
    }

    private function getPayload(?string $username, ?array $roles, ?int $expiresAt): array
    {
        $payload = [];

        if ($username) {
            $payload["username"] = $username;
        }
        if ($roles) {
            $payload["roles"] = $roles;
        }
        if ($roles) {
            $payload["expires_at"] = $expiresAt;
        }

        return $payload;
    }
}