<?php declare(strict_types=1);

namespace Fittinq\Symfony\Authenticator\Authenticator;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class JWTUserProvider implements UserProviderInterface
{
    private string $publicKeyPath;

    public function __construct(string $publicKeyPath)
    {
        $this->publicKeyPath = $publicKeyPath;
    }

    /**
     * @codeCoverageIgnore
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        return $user;
    }

    /**
     * @codeCoverageIgnore
     */
    public function supportsClass(string $class): bool
    {
        return User::class === $class;
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        $jwt = JWT::decode($identifier,  new Key(file_get_contents($this->publicKeyPath), 'RS256'));

        if ($this->jwtIsInvalid($jwt)) {
            throw new UserNotFoundException();
        }

        return new User($jwt->username, $jwt->roles);
    }

    /**
     * @param object $jwt
     * @return bool
     */
    private function jwtIsInvalid(object $jwt): bool
    {
        return !isset($jwt->username, $jwt->roles, $jwt->expires_at) || time() > $jwt->expires_at;
    }

}
