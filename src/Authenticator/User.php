<?php declare(strict_types=1);

namespace Fittinq\Symfony\Authenticator\Authenticator;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    private string $username;
    protected array $roles;

    public function __construct(string $username, array $roles)
    {
        $this->username = $username;
        $this->roles = $roles;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @codeCoverageIgnore
     */
    public function eraseCredentials()
    {
        // This is not needed for the user being a self validating user.
    }

    /**
     * @codeCoverageIgnore
     */
    public function getUserIdentifier(): string
    {
        return $this->username;
    }
}
