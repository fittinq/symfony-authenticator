<?php declare(strict_types=1);

namespace Fittinq\Symfony\Authenticator\Controller;

use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CallbackController
{
    public function callback(Request $request): Response
    {
        $uri = $request->get('uri', $request->getSchemeAndHttpHost());

        $localHost = parse_url($request->getSchemeAndHttpHost(), PHP_URL_HOST);
        $callbackHost = parse_url($uri, PHP_URL_HOST);

        if ($localHost !== $callbackHost) {
            $uri = $request->getSchemeAndHttpHost();
        }

        $response = new RedirectResponse($uri);
        $cookie = Cookie::create('jwt', $request->get('jwt', ''), 0, "/", null, null, false);
        $response->headers->setCookie($cookie);

        return $response;
    }
}
